package zadanie1;

import java.io.IOException;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/schedule")

public class InterestServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	
public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException{
	float loan = Float.parseFloat(request.getParameter("loan"));
	float installmentAmount = Float.parseFloat(request.getParameter("installmentAmount"));
	float bankrate = Float.parseFloat(request.getParameter("bankrate"));
	float fixedprice = Float.parseFloat(request.getParameter("fixedprice"));
	String type = request.getParameter("type");
	
	response.setContentType("text/html");
	response.getWriter().println("<h1>Twoj harmonogram:</h1><table = border = '1'><tr><td>Numer raty</td><td>Kwota kapitalu</td><td>Kwota odsetek</td><td>Oplaty stale</td><td>Calkowita kwota raty</td></tr>");
	
	if ("decreasing".equals(type)){
		float interest = loan*((bankrate/100)/12);
		float capital = loan/installmentAmount;
		float wholeInstallment = interest + capital+fixedprice;
	   
	   for (int i = 1; i<=installmentAmount; i++){
       response.getWriter().println("<tr><td>" + i + "</td><td>"+ Math.round(capital*100f)/100f + "</td><td>" +  Math.round(interest*100f)/100f + "</td><td>" + Math.round(fixedprice*100f)/100f + "</td><td>" + Math.round(wholeInstallment*100f)/100f + "</td></tr>");
	   loan -= capital;
	   interest = loan*((bankrate/100)/12);
	   wholeInstallment = interest + capital+fixedprice;

	     }
	   
	}else if ("static".equals(type) ){
	
	
	double q =  1+((bankrate/100)/12);
	float potegaq =  (float) Math.pow(q, installmentAmount);
	float installment = (float) (loan*potegaq*(q-1)/(potegaq-1));	
	float interest = (float) (loan* (bankrate/100)* (30.0/360.0));
	float capital = installment - interest;
	float wholeInstallment = interest + capital+fixedprice;
	
	for (int i = 1; i<=installmentAmount; i++){
	response.getWriter().println("<tr><td>" + i + "</td><td>"+ Math.round(capital*100f)/100f + "</td><td>" +  Math.round(interest*100f)/100f + "</td><td>" + Math.round(fixedprice*100f)/100f + "</td><td>" + Math.round(wholeInstallment*100f)/100f + "</td></tr>");
	loan -= capital;
	interest = (float) (loan* (bankrate/100)* (30.0/360.0));
	capital = installment - interest;
	wholeInstallment = interest + capital+fixedprice;
	}
}
	response.getWriter().println("</table>");
}
}
