package zadanie1;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Test;
import org.mockito.Mockito;

public class InterestServletTest extends Mockito {

	@Test
	public void servlet_should_not_accept_loan_equal_to_null() throws IOException {
		HttpServletRequest request = mock(HttpServletRequest.class);
		HttpServletResponse response = mock(HttpServletResponse.class);
		PrintWriter writer = mock(PrintWriter.class);
		when(response.getWriter()).thenReturn(writer);
		InterestServlet servlet = new InterestServlet();
		
		when(request.getParameter("loan")).thenReturn(null);
		servlet.doPost(request, response);
		verify(response).sendRedirect("/");
	}
	
	@Test
	public void servlet_should_not_accept_loan_ungiven() throws IOException {
		HttpServletRequest request = mock(HttpServletRequest.class);
		HttpServletResponse response = mock(HttpServletResponse.class);
		PrintWriter writer = mock(PrintWriter.class);
		when(response.getWriter()).thenReturn(writer);
		InterestServlet servlet = new InterestServlet();
		
		when(request.getParameter("loan")).thenReturn("");
		servlet.doPost(request, response);
		verify(response).sendRedirect("/");
	}

	@Test
	public void servlet_should_not_accept_installment_amount_equal_to_null() throws IOException {
		HttpServletRequest request = mock(HttpServletRequest.class);
		HttpServletResponse response = mock(HttpServletResponse.class);
		PrintWriter writer = mock(PrintWriter.class);
		when(response.getWriter()).thenReturn(writer);
		InterestServlet servlet = new InterestServlet();
		
		when(request.getParameter("installmentAmount")).thenReturn(null);
		servlet.doPost(request, response);
		verify(response).sendRedirect("/");
	}
	
	@Test
	public void servlet_should_not_accept_installment_amount_ungiven() throws IOException {
		HttpServletRequest request = mock(HttpServletRequest.class);
		HttpServletResponse response = mock(HttpServletResponse.class);
		PrintWriter writer = mock(PrintWriter.class);
		when(response.getWriter()).thenReturn(writer);
		InterestServlet servlet = new InterestServlet();
		
		when(request.getParameter("installmentAmount")).thenReturn("");
		servlet.doPost(request, response);
		verify(response).sendRedirect("/");
	}

	@Test
	public void servlet_should_not_accept_bank_rate_equal_to_null() throws IOException {
		HttpServletRequest request = mock(HttpServletRequest.class);
		HttpServletResponse response = mock(HttpServletResponse.class);
		PrintWriter writer = mock(PrintWriter.class);
		when(response.getWriter()).thenReturn(writer);
		InterestServlet servlet = new InterestServlet();
		
		when(request.getParameter("bankRate")).thenReturn(null);
		servlet.doPost(request, response);
		verify(response).sendRedirect("/");
	}
	
	@Test
	public void servlet_should_not_accept_bank_rate_ungiven() throws IOException {
		HttpServletRequest request = mock(HttpServletRequest.class);
		HttpServletResponse response = mock(HttpServletResponse.class);
		PrintWriter writer = mock(PrintWriter.class);
		when(response.getWriter()).thenReturn(writer);
		InterestServlet servlet = new InterestServlet();
		
		when(request.getParameter("bankRate")).thenReturn("");
		servlet.doPost(request, response);
		verify(response).sendRedirect("/");
	}

	@Test
	public void servlet_should_not_accept_fixed_price_equal_to_null() throws IOException {
		HttpServletRequest request = mock(HttpServletRequest.class);
		HttpServletResponse response = mock(HttpServletResponse.class);
		PrintWriter writer = mock(PrintWriter.class);
		when(response.getWriter()).thenReturn(writer);
		InterestServlet servlet = new InterestServlet();
		
		when(request.getParameter("fixedPrice")).thenReturn(null);
		servlet.doPost(request, response);
		verify(response).sendRedirect("/");
	}
	
	@Test
	public void servlet_should_not_accept_fixed_price_ungiven() throws IOException {
		HttpServletRequest request = mock(HttpServletRequest.class);
		HttpServletResponse response = mock(HttpServletResponse.class);
		PrintWriter writer = mock(PrintWriter.class);
		when(response.getWriter()).thenReturn(writer);
		InterestServlet servlet = new InterestServlet();
		
		when(request.getParameter("fixedPrice")).thenReturn("");
		servlet.doPost(request, response);
		verify(response).sendRedirect("/");
	}

	@Test
	public void servlet_should_not_work_when_interest_type_not_picked() throws IOException {
		HttpServletRequest request = mock(HttpServletRequest.class);
		HttpServletResponse response = mock(HttpServletResponse.class);
		PrintWriter writer = mock(PrintWriter.class);
		when(response.getWriter()).thenReturn(writer);
		InterestServlet servlet = new InterestServlet();
		
		when(request.getParameter("type")).thenReturn(null);
		servlet.doPost(request, response);
		verify(response).sendRedirect("/");
	}
	
	@Test
	public void servlet_should_work_properly_for_decreasing_installments() throws IOException {
		HttpServletRequest request = mock(HttpServletRequest.class);
		HttpServletResponse response = mock(HttpServletResponse.class);
		PrintWriter writer = mock(PrintWriter.class);
		
		when(request.getParameter("loan")).thenReturn("600");
		when(request.getParameter("installmentAmount")).thenReturn("5");;
		when (request.getParameter("bankrate")).thenReturn("5");;
		when(request.getParameter("fixedprice")).thenReturn("19");
		when(request.getParameter("type")).thenReturn("decreasing");
		new InterestServlet().doPost(request, response);
		
		verify(writer).println("<h1>Twoj harmonogram:</h1><table = border = '1'><tr><td>Numer raty</td><td>Kwota kapitalu</td><td>Kwota odsetek</td><td>Oplaty stale</td><td>Calkowita kwota raty</td></tr>"+
				"<tr><td>" + 1 + "</td><td>"+ 119.0 + "</td><td>" +  2.5 + "</td><td>" + 9.0 + "</td><td>" + 130.5 + "</td></tr>"+
				"<tr><td>" + 2 + "</td><td>"+ 119.5 + "</td><td>" +  2.0 + "</td><td>" + 9.0 + "</td><td>" + 130.5 + "</td></tr>"+
				"<tr><td>" + 3 + "</td><td>"+ 120.0 + "</td><td>" +  1.51 + "</td><td>" + 9.0 + "</td><td>" + 130.5 + "</td></tr>"+
				"<tr><td>" + 4 + "</td><td>"+ 120.5 + "</td><td>" +  1.01 + "</td><td>" + 9.0 + "</td><td>" + 130.5 + "</td></tr>"+
				"<tr><td>" + 5 + "</td><td>"+ 121.0 + "</td><td>" +  0.5 + "</td><td>" + 9.0 + "</td><td>" + 130.5 + "</td></tr></table>");

	}
	

	@Test
	public void servlet_should_work_properly_for_fixed_installments() throws IOException {
		HttpServletRequest request = mock(HttpServletRequest.class);
		HttpServletResponse response = mock(HttpServletResponse.class);
		PrintWriter writer = mock(PrintWriter.class);
		
		when(request.getParameter("loan")).thenReturn("600");
		when(request.getParameter("installmentAmount")).thenReturn("5");;
		when (request.getParameter("bankrate")).thenReturn("5");;
		when(request.getParameter("fixedprice")).thenReturn("19");
		when(request.getParameter("type")).thenReturn("static");
		new InterestServlet().doPost(request, response);
		
		verify(writer).println("<h1>Twoj harmonogram:</h1><table = border = '1'><tr><td>Numer raty</td><td>Kwota kapitalu</td><td>Kwota odsetek</td><td>Oplaty stale</td><td>Calkowita kwota raty</td></tr>"+
				"<tr><td>" + 1 + "</td><td>"+ 120.0 + "</td><td>" +  2.5 + "</td><td>" + 9.0 + "</td><td>" + 131.5 + "</td></tr>"+
				"<tr><td>" + 2 + "</td><td>"+ 120.0 + "</td><td>" +  2.0 + "</td><td>" + 9.0 + "</td><td>" + 131.0 + "</td></tr>"+
				"<tr><td>" + 3 + "</td><td>"+ 120.0 + "</td><td>" +  1.5 + "</td><td>" + 9.0 + "</td><td>" + 130.5 + "</td></tr>"+
				"<tr><td>" + 4 + "</td><td>"+ 120.0 + "</td><td>" +  1.0 + "</td><td>" + 9.0 + "</td><td>" + 130.0 + "</td></tr>"+
				"<tr><td>" + 5 + "</td><td>"+ 120.0 + "</td><td>" +  0.5 + "</td><td>" + 9.0 + "</td><td>" + 129.5 + "</td></tr></table>");

	}
	
}
